require('dotenv').config();
const { Client, Intents } = require('discord.js');
const schedule = require('node-schedule');
const moment = require('moment');
const fetch = (...args) => import('node-fetch').then(
    ({ default: fetch }) => fetch(...args));

const client = new Client({ intents: [Intents.FLAGS.GUILDS] });

let ayodhyaWeather = null;

const ayodhyaWeatherRule = new schedule.RecurrenceRule();
ayodhyaWeatherRule.hour = 6;

const weatherFetchingJob = schedule.scheduleJob(ayodhyaWeatherRule, () => {
    fetchAyodhyaWeather();
});

let morningGreetingJob = null;
let eveningGreetingJob = null;

client.once('ready', () => {
    console.log('Client Ready!');
});

client.on('guildCreate', (guild) => {
    guild.channels.create('greetings', {
        reason: 'Needed greetings channel to spread bhakti.',
    })
        .then(() => console.log(`Greetings Channel Created for ${guild.name}`))
        .catch(error => console.log(error));
});

client.on('disconnect', () => {
    weatherFetchingJob.cancel();
    morningGreetingJob.cancel();
    eveningGreetingJob.cancel();
    schedule.gracefulShutdown();
});

client.login(process.env.botToken)
    .then(() => {
        console.log('Login successful');
        fetchAyodhyaWeather();
    })
    .catch((error) => console.log(error));

async function fetchAyodhyaWeather() {
    ayodhyaWeather = await fetch(
        'https://api.sunrise-sunset.org/json?lat=26.795783948933074&lng=82.19467386192275')
        .then(response => response.json())
        .catch(error => console.log(error));

    const channel = client.channels.cache.find(
        channelName => channelName.name === 'greetings');

    if (ayodhyaWeather != null) {
        const morningDate = moment.utc(ayodhyaWeather.results.sunrise,
            'hh:mm:ss A').utcOffset('+5:30');
        const eveningDate = moment.utc(ayodhyaWeather.results.sunset,
            'hh:mm:ss A').utcOffset('+5:30');

        const morningDateRule = new schedule.RecurrenceRule();
        morningDateRule.hour = morningDate.hour();
        morningDateRule.minute = morningDate.minute();
        morningDateRule.second = morningDate.second();

        const eveningDateRule = new schedule.RecurrenceRule();
        eveningDateRule.hour = eveningDate.hour();
        eveningDateRule.minute = eveningDate.minute();
        eveningDateRule.second = eveningDate.second();

        morningGreetingJob = schedule.scheduleJob(morningDateRule, () => {
            channel.send('Good morning. Jay Shree Ram!');
        });
        eveningGreetingJob = schedule.scheduleJob(eveningDateRule, () => {
            channel.send('Good night. Jay Shree Ram!');
        });
    }

}

// Invite link:
// https://discord.com/api/oauth2/authorize?client_id=945963196756414475&permissions=8&scope=bot%20applications.commands
